# ESP8266 AWS iot exemple

This is an exemple to connect AWS Iot core With an arduino esp8266 through MQTT. The code I'm using is entirely copied from the [ESP-MQTT-AWS-IoT-Core](https://github.com/debsahu/ESP-MQTT-AWS-IoT-Core) project. I'm just making it easier to use it as I had some troubles making it working.

## Getting Started

These instructions will get you a copy of the project up and running on your Esp8266 for development and testing purposes. You will first need to [set up the arduino IDE to work with Esp8266.](https://www.instructables.com/id/Setting-Up-the-Arduino-IDE-to-Program-the-ESP8266-/)

You will also need to create a thing in your AWS IOT core console. Please refer to [these instructions](https://github.com/debsahu/ESP-MQTT-AWS-IoT-Core/blob/master/doc/README.md) to set up your thing. Make sure to save the certificates that you will create, we will need them in the future.

### Prerequisites

After you've set up the arduino IDE to work with the esp8266 you will need to download some librarys from the Arduino library manager.

First install the [MQTT library](https://github.com/256dpi/arduino-mqtt) from Joel Gahewiler :

![ALT](/imgs/MQTT.png)

Then, install the [ArduinoJson library](https://arduinojson.org/?utm_source=meta&utm_medium=library.properties) from Benoit Blanchon 

![ALT](/imgs/ArduinoJson.png)

### Installing

After that you have dowloaded the librarys succesfuly you can download this project as a zip file or with the command :
```
git clone https://gitlab.com/aim_erisson/esp8266-aws-iot-exemple.git
```
## Code set up

In this section we will set up or secret.h file to connect our esp with AWS iot Core.

### Wifi set up
```
// secrets.h

const char ssid[] = "SSID"; <-- Here enter your wifi ssid 
const char pass[] = "PASS"; <-- Here enter your wifi password 
```
### AWS set up
Here we will set up our AWS thing, endpoint and credentials. 

```
#define THINGNAME "WelcomeTest" <-- Here enter your thing name that you created earlier. 

const char MQTT_HOST[] = "xxxxxxxxx.iot.us-east-1.amazonaws.com"; <-- Here enter your AWS endpoint, you can find it in your Aws Iot core settings tab.
```

After that you will only need to copy past your credentials (the one you dowloaded earlier). 

### Testing the code

After making sure that you've set up the code sucessfuly you can upload the code on your esp8266 with the arduino IDE.

If everything works you should get this result in the Arduino Monitor :

![ALT](/imgs/test.png)

## Acknowledgments

* Thanks to Debashish Sahu and his work on the [ESP-MQTT-AWS-IoT-Core](https://github.com/debsahu/ESP-MQTT-AWS-IoT-Core) project.


